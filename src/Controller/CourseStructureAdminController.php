<?php

declare(strict_types=1);

namespace Drupal\trinion_suo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

final class CourseStructureAdminController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke(Node $node): array {
    $cours_tid = $node->get('field_ts_kategoriya_kursa')->getValue()[0]['target_id'];
    $categories = \Drupal::service("trinion_suo.course")->getCategories($cours_tid);
    $rows = [];
    foreach ($categories as $cat) {
      $rows[] = [
        'data' => [
          $cat['name'],
          [
            'data' => $cat['term']->getWeight(),
            'class' => ['inline-change'],
            'data-field' => 'weight',
            'id' => 'weight-' . $cat['term']->id()
          ],
          $cat['length'],
          ['colspan' => 7]
        ],
        'data-id' => $cat['term']->id(),
        'data-type' => 'taxonomy_term',
      ];
      foreach ($cat['sub_categories'] as $sub_cat) {
        $rows[] = [
          'data' => [
            [
              'data' => '',
              'class' => ['inline-change'],
              'data-field' => 'category',
              'id' => 'category-' . $sub_cat['term']->id()
            ],
            ['colspan' => 2],
            $sub_cat['name'],
            [
              'data' => $sub_cat['term']->getWeight(),
              'class' => ['inline-change'],
              'data-field' => 'weight',
              'id' => 'weight-' . $sub_cat['term']->id()
            ],
            $sub_cat['length'],
            ['colspan' => 4],
          ],
          'data-id' => $sub_cat['term']->id(),
          'data-type' => 'taxonomy_term',
        ];
        foreach ($sub_cat['lessons'] as $less) {
          if ($less->bundle() == 'urok_kursa') {
            if ($less->get('field_ts_vid_uroka')->getString() == 'Видео')
              $less_type = 'video';
            elseif ($less->get('field_ts_vid_uroka')->getString() == 'Практическое задание')
              $less_type = 'praktika';
            else
              $less_type = 'text';
          }
          else {
            $less_type = 'test';
          }
          $rows[] = [
            'data' => [
              ['colspan' => 3],
              [
                'data' => '',
                'class' => ['inline-change'],
                'data-field' => 'lesson',
                'id' => 'lesson-' . $less->id()
              ],
              ['colspan' => 2],
              $less_type,
              $less->label(),
              [
                'data' => $less->get('field_ts_lesson_number')->getString(),
                'class' => ['inline-change'],
                'data-field' => 'field_ts_lesson_number',
                'id' => 'field_ts_lesson_number-' . $less->id()
              ],
              [
                'data' => $less->hasField('field_ts_length') ? $less->get('field_ts_length')->getString() : 0,
                'class' => [$less->hasField('field_ts_length') ? 'inline-change' : ''],
                'data-field' => 'field_ts_length',
                'id' => 'field_ts_length-' . $less->id()
              ],
            ],
            'data-id' => $less->id(),
            'data-type' => 'node',
          ];
        }
      }
    }

    $build['content'][] = [
      '#theme' => 'table',
      '#header' => ['Категория', 'Вес', 'svg_clock', 'Занятие', 'Вес', 'svg_clock', '', 'Урок', 'Вес', 'svg_clock', ],
      '#rows' => $rows,
      '#attributes' => [
        'border' => 0,
        'class' => ['t-changed-table'],
        'data-course-structure-table' => TRUE,
      ],
    ];

    return $build;
  }

  public function getTitle(Node $node) {
    return $node->label();
  }
}

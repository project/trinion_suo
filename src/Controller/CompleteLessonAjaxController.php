<?php

namespace Drupal\trinion_suo\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Returns responses for TrinionCourse routes.
 */
class CompleteLessonAjaxController extends ControllerBase {
  /**
   * Builds the response.
   */
  public function response(Node $node) {
    $nid = $node->id();
    if ($uid = \Drupal::currentUser()->id()) {
      $user = User::load($uid);
      $user->field_ts_completed_lessons[] = $nid;
      $user->save();
    }
    $next_nid = \Drupal::service('trinion_suo.course')->getNextLesson($node);

    $response = new AjaxResponse();
    if ($next_nid)
      $response->addCommand(new RedirectCommand('/node/' . $next_nid));

    return $response;
  }
}

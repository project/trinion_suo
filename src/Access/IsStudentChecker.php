<?php

namespace Drupal\trinion_suo\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\user\Entity\User;
use Symfony\Component\Routing\Route;

class IsStudentChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route) {
    $user = User::load(\Drupal::currentUser()->id());
    return AccessResult::allowedIf($user->get('field_ts_uchenik')->getString());
  }
}

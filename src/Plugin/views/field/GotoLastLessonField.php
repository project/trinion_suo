<?php

namespace Drupal\trinion_suo\Plugin\views\field;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Course last lesson link field handler.
 *
 * @ViewsField("goto_last_lesson")
 */
class GotoLastLessonField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $course = \Drupal::service('trinion_suo.course')->getCategories($values->taxonomy_term_field_data_node__field_ts_kategoriya_kursa_tid);
    foreach ($course as $cat) {
      foreach ($cat['sub_categories'] as $cat) {
        foreach ($cat['lessons'] as $les) {
          if (!$les->completed) {
            return [
              '#type' => 'link',
              '#title' => t('Continue'),
              '#url' => Url::fromRoute('entity.node.canonical', ['node' => $les->id()]),
            ];
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }
}

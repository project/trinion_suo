<?php

namespace Drupal\trinion_suo\Plugin\views\access;

use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides no access control at all.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "is_ichenik",
 *   title = @Translation("Is student"),
 *   help = @Translation("Will be available to course students.")
 * )
 */
class IsUchenik extends AccessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->t('Is Student');
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_custom_access', 'access_check.trinion_suo.is_student::access');
  }

}

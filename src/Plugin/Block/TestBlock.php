<?php

namespace Drupal\trinion_suo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a test block.
 *
 * @Block(
 *   id = "trinion_suo_test",
 *   admin_label = @Translation("Test"),
 *   category = @Translation("Custom")
 * )
 */
class TestBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\trinion_suo\Form\TestForm');
    $build['content'] = $form;
    return $build;
  }

}

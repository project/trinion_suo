<?php

namespace Drupal\trinion_suo\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a course plan block.
 *
 * @Block(
 *   id = "trinion_suo_course_plan",
 *   admin_label = @Translation("Course plan"),
 *   category = @Translation("Custom")
 * )
 */
class CoursePlanBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [];
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node) {
      switch ($node->bundle()) {
        case 'test':
          $course_tid = $node->get('field_ts_kategoriya_kursa')->getString();
          $cat = Term::load($course_tid);
          $course_tid = $cat->get('parent')->getString();
          break;
        case 'urok_kursa':
          $course_tid = $node->get('field_ts_kategoriya_kursa')->getString();
          $cat = Term::load($course_tid);
          $course_tid = $cat->get('parent')->getString();
          break;
      }
      if (isset($course_tid) && $course_tid) {
        $parents = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($course_tid);
        $course = array_pop($parents);
        $categories = \Drupal::service("trinion_suo.course")->getCategories($course->id());
        $build['content'] = [
          '#theme' => 'course_plan_menu',
          '#categories' => $categories[$course_tid],
        ];
      }
    }

    return $build;
  }

}

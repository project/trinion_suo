<?php

namespace Drupal\trinion_suo;


use Drupal\Core\Access\AccessResult;
use Drupal\node\Entity\Node;
use Drupal\node\Plugin\views\filter\Access;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

class Course {
  const COURSE_VID = 'course_categories';

  public function getCategories($tid) {
    $data = [];
    $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree(self::COURSE_VID, $tid, 1, true);
    if ($current_lesson_node = \Drupal::routeMatch()->getParameter('node'))
      $current_lesson_nid = $current_lesson_node->id();
    else
      $current_lesson_nid = FALSE;
    foreach ($tree as $category) {
      $sub_tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree(self::COURSE_VID, $category->id(), 1, true);
      $sub_categories = [];
      $category_length = 0;
      foreach ($sub_tree as $sub_cat) {

        $lessons = $this->getLessons($sub_cat->id());
        $length = 0;
        $completed = 0;
        $current = FALSE;
        foreach ($lessons as $lesson) {
          $is_completed = $this->isLessonCompleted($lesson);
          if ($is_completed)
            $completed += 1;
          $lesson->completed = $is_completed;
          $lesson->current_lesson = $lesson->id() == $current_lesson_nid;
          if ($lesson->current_lesson)
            $current = TRUE;
          $lesson->opened = $lesson->access('view', \Drupal::currentUser());
          if ($lesson->status->getString()) {
            if ($len = $lesson->get('field_ts_length')->first()) {
              $lesson_length = $len->getValue()['value'];
              $length += $lesson_length;
              $category_length += $lesson_length;
            }
          }
        }
        $sub_categories[] = [
          'name' => $sub_cat->getName(),
          'term' => $sub_cat,
          'lessons' => $lessons,
          'length' => $length,
          'completed' => $completed,
          'have_current_lesson' => $current,
        ];
      }

      $cat = [
        'name' => $category->getName(),
        'lessons' => $this->getLessons($category->id()),
        'term' => $category,
        'length' => $category_length,
        'sub_categories' => $sub_categories,
      ];
      $data[$category->id()] = $cat;
    }
    return $data;
  }

  public function getLessons($tid) {
    $query = \Drupal::database()->select('node__field_ts_kategoriya_kursa', 'n')
      ->condition('n.field_ts_kategoriya_kursa_target_id', $tid);
    $query->join('node_field_data', 'node', 'node.nid = n.entity_id');
    $query->condition('node.type', ['urok_kursa', 'test'], 'IN');
    $query->leftJoin('node__field_ts_lesson_number', 'l', 'l.entity_id = n.entity_id');
    $query->addExpression('CAST(l.field_ts_lesson_number_value AS UNSIGNED)', 'num');
    $query->addField('n', 'entity_id');
    $query->orderBy('num');
    $nids = $query->execute();

    $lessons = [];
    foreach ($nids as $record) {
      $node = Node::load($record->entity_id);
      $lessons[$record->entity_id] = $node;
    }
    return $lessons;
  }

  /**
   * Получение следующего урока
   * @param $node
   *
   * @return false
   */
  function getNextLesson($node) {
    $nid = $node->id();
    $lessons = $this->getLessons($node->get('field_ts_kategoriya_kursa')->getString());
    $next_nid = FALSE;
    foreach ($lessons as $lesson) {
      if (!empty($get_next)) {
        $next_nid = $lesson->id();
        break;
      }
      if ($nid == $lesson->id()) {
        $get_next = TRUE;
      }
    }
    return $next_nid;
  }

  /**
   * Проверка, пройден ли урок
   *
   * @param $node
   */
  public function isLessonCompleted($node) {
    $query = \Drupal::entityQuery('user')
      ->condition('uid', \Drupal::currentUser()->id())
      ->condition('field_ts_completed_lessons', $node->id())
      ->accessCheck(TRUE);
    return $query->execute();
  }

  public function getFirstCourseLesson($tid) {
    $query = \Drupal::database()->select('taxonomy_term__parent', 'p')
      ->condition('p.parent_target_id', $tid);
    $query->join('taxonomy_term_field_data', 'd', 'd.tid = p.entity_id');
    $query->orderBy('d.weight', 'ASC');
    $query->orderBy('d.tid', 'ASC');
    $query->join('node__field_ts_kategoriya_kursa', 'k', 'k.field_ts_kategoriya_kursa_target_id = d.tid');
    $query->condition('k.bundle', ['urok_kursa', 'test'], 'IN');
    $query->addField('k', 'entity_id');
    $res = $query->execute();
    return $res->fetchField();
  }

  public function checkLessonAccess($entity) {
    if ($entity->hasField('field_ts_free_lesson')) {
      $is_free = $entity->get('field_ts_free_lesson')->getValue();
      if (!empty($is_free[0]['value']))
        return TRUE;
    }
    $course_category_tid = $entity->get('field_ts_kategoriya_kursa')->getValue()[0]['target_id'];
    $course_sub_category = Term::load($course_category_tid);
    $course_category = Term::load($course_sub_category->get('parent')->getValue()[0]['target_id']);
    $course = Term::load($course_category->get('parent')->getValue()[0]['target_id']);

    return $this->checkCourseAccess($course);
  }

  public function checkCourseAccess($course) {
    $user = User::load(\Drupal::currentUser()->id());
    $course_access = $user->get('field_ts_course_access')->getValue();
    foreach ($course_access as $access) {
      if ($access['target_id'] == $course->id())
        return TRUE;
    }
    return FALSE;
  }

  public function getCourseProgress($categories) {
    $completed_cnt = 0;
    $uid = \Drupal::currentUser()->id();
    if ($uid) {
      $total_cnt = 0;
      $user = User::load($uid);
      $completed_nids = explode(',', $user->get('field_ts_completed_lessons')->getString());
      foreach ($categories as $cat) {
        foreach ($cat['lessons'] as $lesson_nid => $lesson) {
          $total_cnt++;
          if (in_array($lesson_nid, $completed_nids))
            $completed_cnt++;
        }
      }
    }
    else
      $total_cnt = 1;
    return ceil((100 * $completed_cnt) / $total_cnt);
  }
}

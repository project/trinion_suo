<?php

namespace Drupal\trinion_suo\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Определение темы для SUO
 */
class TrinionSuoNegotiator implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route_name = $route_match->getRouteName();
    if (in_array($route_name, ['trinion_suo.course_structure_admin', 'view.suo_view_course.page_1', 'view.suo_view_course.page_2', 'view.suo_user_courses.page_1', ]))
      return TRUE;
    if ($route_name == 'entity.node.canonical') {
      $bundle = $route_match->getParameter('node')->bundle();
      if (in_array($bundle, ['urok_kursa', 'test', ]))
        return TRUE;
      if ($bundle == 'kurs_obucheniya') {
        $user =  User::load(\Drupal::currentUser()->id());
        return $user->get('field_ts_uchenik')->getString();
      }
    }
    elseif ($route_name == 'entity.taxonomy_term.canonical') {
      /** @var Term $term */
      $term = $route_match->getParameter('taxonomy_term');
      if ($term->bundle() == 'course_categories') {
        $user =  User::load(\Drupal::currentUser()->id());
        return $user->get('field_ts_uchenik')->getString();
      }
    }
    $request_uri = \Drupal::request()->getRequestUri();
    if (preg_match('/^\/obuchenie(\/|$)/', $request_uri))
      return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $theme_name = \Drupal::config('trinion_base.settings')->get('backend_theme_name');
    if (is_null($theme_name))
      $theme_name = 'trinion_backend';
    return $theme_name;
  }
}
